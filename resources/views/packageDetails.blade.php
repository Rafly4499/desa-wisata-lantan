@extends('layouts.frontend.master')
@section('title')
Desa Wisata Lantan - {{ $package->name }}
@endsection

@section('css')

@endsection



@section('content')
<div class="breadcrumb-area style-two jarallax"
    style="background-image:url({{ asset('frontend/assets/img/bg/1.png') }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Package Details</h1>
                    <ul class="page-list">
                        <li><a href="/">Home</a></li>
                        <li>Package Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container my-5" style="padding-top: 120px">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h2><strong>Package Details: </strong></h2>
                    <a href="{{ route('welcome') }}" class="btn btn-danger">Back to home</a>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ asset('storage/packageImage/'.$package->package_image) }}" alt="img">
    <div class="row">

        <table class="table my-3">
            <tr>
                <th>Package Name</th>
                <td>{{ $package->name }}</td>
            </tr>
            <tr>
                <th>Package Added By</th>
                <td>{{ $package->added_by }}</td>
            </tr>
            <tr>
                <th>Places</th>
                <td>
                    @foreach ($package->places as $place)
                    <span style="background: orange; color:black" class="px-3 py-2 m-2">
                        <strong>{{ $place->name }}</strong>
                    </span>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th>Package Price</th>
                <td>{{ $package->price }}</td>
            </tr>
            <tr>
                <th>People</th>
                <td>{{ $package->people }}</td>
            </tr>
            <tr>
                <th>Time</th>
                <td>{{ $package->day }}</td>
            </tr>
        </table>
        <br>
        <h3 class="my-5" style="color: whitesmoke; background-color: black; padding:12px;">Description & rules: </h3>
        <div style="text-align: justify"> {!! $package->description !!}</div>
    </div>
</div>




@endsection

@section('scripts')

@endsection

@section('css')

@endsection