<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title')</title>
  <!-- favicon -->
  <link rel=icon href="{{ asset('assets/img/favicon.png') }}" sizes="20x20" type="image/png">

  <!-- Additional plugin css -->
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/magnific-popup.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/slick.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/swiper.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/nice-select.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/jquery-ui.min.css') }}">
  <!-- icons -->
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/line-awesome.min.css') }}">
  <!-- main css -->
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/style.css') }}">
  <!-- responsive css -->
  <link rel="stylesheet" href="{{ asset('frontend/assets/css/responsive.css') }}">
  @yield('css')
</head>

<body>
  <div class="preloader" id="preloader">
    <div class="preloader-inner">
      <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
      </div>
    </div>
  </div>
  <!-- Navbar -->
  @include('layouts.frontend.inc.nav')


  <!-- Main content -->
  @yield('content')


  {{-- footer --}}
  @include('layouts.frontend.inc.footer')

  <script src="{{ asset('frontend/assets/js/jquery-2.2.4.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/popper.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/jquery.magnific-popup.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/wow.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/slick.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/waypoints.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/jquery.counterup.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/imagesloaded.pkgd.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/swiper.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/jarallax.min.js') }}"></script>

  <!-- main js -->
  <script src="{{ asset('frontend/assets/js/main.js') }}"></script>
  @yield('scripts')

</body>

</html>