<footer class="footer-area style-three" style="background-image: url({{ asset('frontend/assets/img/bg/2.png') }});">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="about_us_widget text-center">
          <a href="index.html" class="footer-logo">
            <img src="{{ asset('assets/img/logo2.png') }}" alt="footerlogo">
          </a>
        </div>
        <div class="footer-widget widget text-center">
          <ul class="widget_nav_menu text-center">
            <li><a href="{{ route('welcome') }}">Home</a></li>
            <li><a href="{{ route('about') }}">About Us</a></li>
            <li><a href="{{ route('all.place') }}">Places</a></li>
            <li><a href="{{ route('all.package') }}">Packages</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-lg-12">
        <div class="footer-widget widget text-center">
          <div class="widget-contact d-inline-flex">
            <p class="telephone text-left">
              <i class="fa fa-phone base-color"></i>
              <span>
                087750677424
              </span>
            </p>
            <p class="location text-left">
              <i class="fa fa-envelope-o"></i>
              <span>pokdarwissolah@gmail.com</span>
            </p>
            <p class="text-left">
              <i class="fa fa-map-marker"></i>
              <span>Kecamatan Batukliang Utara, Kabupaten Lombok Tengah, Nusa Tenggara Barat</span>
            </p>
          </div>
        </div>
      </div>
      {{-- <div class="col-lg-5">
        <div class="widget input-group newslatter-wrap style-two">
          <h2 class="head">Types Of Places</h2>
          @forelse ($placetypes as $placetype)
          <p><i class="fas fa-check"> </i> <a href="{{ route('placetype.wise.place', $placetype->id) }}">{{
              $placetype->name }}</a></p>
          @empty
          <p class="text-center m-auto p-3 text-white bg-dark"><strong>No Placetype found right now</strong></p>
          @endforelse
        </div>
      </div> --}}
    </div>
  </div>
  <div class="copyright-inner border-tp-solid">
    <div class="container">
      <div class="copyright-text text-center">
        &copy; Desa Wisata Lantan 2023 All rights reserved. Development with <i class="fa fa-heart"></i> by <a
          href="http://punggawastudio.com" target="_blank"><span>Punggawa Studio</span></a>
      </div>
    </div>
  </div>
</footer>
<!-- footer area end -->

<!-- back to top area start -->
<div class="back-to-top">
  <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>