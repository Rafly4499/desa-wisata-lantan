<div class="top-navbar">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 topbar-contact-wrap">
        <div class="topbar-contact">
          <i class="fa fa-phone"></i>
          <span class="title">Support:</span>
          <span class="number">087750677424</span>
        </div>
        <ul class="social-icon">
          <li>
            <a class="facebook"
              href="https://www.facebook.com/102250239294794/posts/pfbid0cj4Sc7GU2cgKpD9SnV6zURaZjbdx7qfMkSTWg4vbN4aRHehxCGZ3jFmWgEmh5Mpfl/?sfnsn=wiwspmo&extid=a&mibextid=7htlnv"
              target="_blank"><i class="fa fa-facebook"></i></a>
          </li>
          <li>
            <a class="twitter" href="https://www.instagram.com/desa_wisata_lantan/" target="_blank"><i
                class="fa fa-youtube"></i></a>
          </li>
          <li>
            <a class="pinterest" href="https://www.instagram.com/desa_wisata_lantan/" target="_blank"><i
                class="fa fa-instagram"></i></a>
          </li>
        </ul>
      </div>
      <div class="col-lg-6">
        <div class="nav-right-content float-right">
          <ul class="pl-0">
            <li class="tp-lang">

            </li>
            @guest
            <li class="notification">
              <a href="{{ route('login') }}">
                <i class="fa fa-user-o"></i>
              </a>
            </li>
            @else
            @if (Auth::user()->role_id == 1)
            <li class="tp-lang">
              <div class="tp-lang-wrap">
                <a class="nav-link" href="{{ route('admin.dashboard') }}">Dashboard</a>
              </div>
            </li>
            @endif
            @if (Auth::user()->role_id == 2)
            <li class="tp-lang">
              <div class="tp-lang-wrap">
                <a class="nav-link" href="{{ route('user.dashboard') }}">Dashboard</a>
              </div>
            </li> @endif
            @endguest
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<nav class="navbar navbar-area navbar-expand-lg nav-style-03">
  <div class="container nav-container">
    <div class="responsive-mobile-menu">
      <div class="mobile-logo">
        <a href="/">
          <img src="{{ asset('assets/img/logo2.png') }}" alt="logo">
        </a>
      </div>
      <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#tp_main_menu"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggle-icon">
          <span class="line"></span>
          <span class="line"></span>
          <span class="line"></span>
        </span>
      </button>
      <div class="nav-right-content">
        <ul class="pl-0">
          <li class="top-bar-btn-booking">
            <a class="btn btn-yellow" href="tour-details.html">Book Now <i class="fa fa-paper-plane"></i></a>
          </li>
          <li class="tp-lang">

          </li>
          @guest
          <li class="notification">
            <a href="{{ route('login') }}">
              <i class="fa fa-user-o"></i>
            </a>
          </li>
          @else
          @if (Auth::user()->role_id == 1)
          <li class="tp-lang">
            <div class="tp-lang-wrap">
              <a class="nav-link" href="{{ route('admin.dashboard') }}">Dashboard</a>
            </div>
          </li>
          @endif
          @if (Auth::user()->role_id == 2)
          <li class="tp-lang">
            <div class="tp-lang-wrap">
              <a class="nav-link" href="{{ route('user.dashboard') }}">Dashboard</a>
            </div>
          </li> @endif
          @endguest
        </ul>
      </div>
    </div>
    <div class="collapse navbar-collapse" id="tp_main_menu">
      <div class="logo-wrapper desktop-logo">
        <a href="/" class="main-logo">
          <img src="{{ asset('assets/img/logo2.png') }}" alt="logo">
        </a>
        <a href="/" class="sticky-logo">
          <img src="{{ asset('assets/img/sticky-logo.png') }}" alt="logo">
        </a>
      </div>
      <ul class="navbar-nav">
        <li>
          <a href="{{ route('welcome') }}">Home</a>
        </li>
        <li>
          <a href="{{ route('about') }}">About Us</a>
        </li>
        <li>
          <a href="{{ route('all.place') }}">Places</a>
        </li>
        <li>
          <a href="{{ route('all.package') }}">Packages</a>
        </li>
      </ul>
    </div>
    <div class="nav-right-content">
      <ul>
        <li class="pr-0">
          <a class="btn btn-yellow" href="{{ route('all.package') }}">Book Now <i class="fa fa-paper-plane"></i></a>
        </li>
      </ul>
    </div>
  </div>
</nav>