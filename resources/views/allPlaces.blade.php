@extends('layouts.frontend.master')
@section('title')
Desa Wisata Lantan - All Places
@endsection

@section('css')
<style>
    .places {
        margin-top: 60px;
        margin-bottom: 60px;
    }
</style>
@endsection



@section('content')
<!-- breadcrumb area start -->
<div class="breadcrumb-area style-two jarallax"
    style="background-image:url({{ asset('frontend/assets/img/bg/1.png') }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Destinations List</h1>
                    <ul class="page-list">
                        <li><a href="/">Home</a></li>
                        <li>Destinations List</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- destination area End -->
<div class="destination-area">
    <div class="container-bg mg-top--70">
        <div class="container">
            <div class="row justify-content-cente">
                @forelse ($places as $place)
                <div class="col-lg-4 col-md-6">
                    <div class="single-destination-grid text-center">
                        <div class="thumb">
                            <img src="{{ asset('storage/place/'. $place->image) }}" alt="img">
                        </div>
                        <div class="details">
                            <h3 class="title">{{ $place->name }}</h3>
                            <p class="content">District: <strong>{{ $place->district->name }}</strong></p>
                            <p class="content">Place Type: <strong>{{ $place->placetype->name }}</strong></p>
                            <a class="btn btn-gray" href="{{ route('place.details', $place->id) }}"><span>Explore<i
                                        class="la la-arrow-right"></i></span></a>
                        </div>
                    </div>
                </div>
                @empty
                <h2 class="my-5 bg-info text-white text-center p-3">No Place Found. Please add some place.</h2>
                @endforelse
            </div>
            <div class="d-flex justify-content-between">
                <div>
                    <a href="{{ route('welcome') }}" class="btn btn-danger my-5">Back to home</a>
                </div>
                <div class="my-5">
                    {{ $places->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection

@section('css')

@endsection