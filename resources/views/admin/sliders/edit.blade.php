@extends('layouts.backend.master')

@section('title')
Sliders
@endsection

@section('content')
<div class="container-fluid">

  <div class="section-body">
    <h2 class="section-title">Sliders</h2>
    <p class="section-lead">
      Edit Sliders
    </p>

    <form action="{{ url('/admin/slides/'.$slider->id) }}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input type="hidden" name="_method" value="PUT">
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-header">
              <h4>Sliders</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label class="form-control-label" for="basic-url">Title Sliders</label>
                <div class="input-group">
                  <input type="text" name="title" value="{{ $slider->title }}" class="form-control"
                    aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" required>
                </div>
              </div>
              <div class="form-group">
                <label class="form-control-label" for="basic-url">Description</label>
                <div class="input-group">
                  <input type="text" name="description" value="{{ $slider->description }}" class="form-control"
                    aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" required>
                </div>
              </div>
              <div class="form-group">
                <label class="form-control-label" for="basic-url">Link Youtube</label>
                <div class="input-group">
                  <input type="text" name="link" value="{{ $slider->link }}" class="form-control"
                    aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" required>
                </div>
              </div>
              <div class="form-group">
                <label class="form-control-label" for="basic-url">Photo</label>
                <div class="input-group">
                  <input type="file" name="photo" class="form-control" value="{{ $slider->photo }}"
                    data-default-file="{{ $slider->photo }}" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default">
                </div>
                <br>
                {{-- <img style="width: 100%" src="{{url('images/sliders/')}}/{{$slider->photo}}" alt="image"> --}}
              </div>
            </div>
            <div class="card-footer text-right">
              <button class="btn btn-primary">Save</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection