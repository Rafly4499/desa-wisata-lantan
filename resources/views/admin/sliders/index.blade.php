@extends('layouts.backend.master')

@section('title')
Sliders
@endsection

@section('content')
<div class="container-fluid">

  <div class="section-body">
    <h2 class="section-title">Sliders</h2>
    <p class="section-lead">
      List Sliders
    </p>

    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h4>Sliders</h4>
          </div>
          <div class="card-body"><a href="{{ url('/admin/slides/create') }}"><button type="button"
                class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i> Insert Data</button></a>
            <br><br>
            <div class="table-responsive">
              <table class="table table-striped table-md" id="table">
                <thead>
                  <tr>
                    <th class="text-center" width="32px">
                      #
                    </th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Photo</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 0;?> @foreach($sliders as $item)
                  <?php $no++;?>
                  <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->description }}</td>
                    <td><img alt="image" src="{{ url('images/sliders/'.$item->photo)}}" style="width: 300px">
                    </td>
                    <td>
                      <form action="{{ url('/admin/slides/'.$item->id) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <a href="{{ url('/admin/slides/'.$item->id.'/edit') }}" class="btn btn-success"><i
                            class="fas fa-pencil-alt"></i> Edit</a>
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i
                            class="fa fa-trash"></i>
                          Delete</button>
                      </form>
                    </td>

                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
@parent
<script type="text/javascript">
  $(document).ready(function(){
    $('#table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});
</script>
@endsection