@extends('layouts.frontend.master')
@section('title')
Desa Wisata Lantan - About
@endsection

@section('css')

@endsection



@section('content')
<!-- This example requires Tailwind CSS v2.0+ -->


<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url({{ url('/assets/img/banner/babakpelangi.png') }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">About Us</h1>
                    <ul class="page-list">
                        <li><a href="/">Home</a></li>
                        <li>About Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->
<div class="about-section pd-top-80">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 align-self-center">
                <div class="section-title mb-lg-0">
                    <h2 class="title"> Lets Go Travel<br> with Us</h2>
                    <p>Lantan memiliki arti kata panjang sesuai dengan bentu geografisnya yang memanjang menuju kaki
                        gunung rinjani. Terdiri dari 10 dusun Lantan memiliki lebih dari 7 air terjun yang airnya
                        bersumber dari kaki gunung rinjani. Tidak hanya itu wisatawan juga dapat menikmati hamparan
                        persawahan dan kegiatan khas masyrakat lantan seperti bertani, berkebun dan berternak.
                </div>
            </div>
            <div class="col-lg-5 offset-lg-2">
                <div class="thumb about-section-right-thumb">
                    <img src="{{ url('/assets/img/desawistalantan.png') }}" alt="img">
                    <img class="about-absolute-thumb" src="{{ url('/assets/img/camping.png') }}" alt="img">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- intro area start -->
<div class="intro-area pd-top-108">
    <div class="container">
        <div class="section-title text-lg-center text-left">
            <h2 class="title"><span>We Are</span> Desa Wisata Lantan</h2>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-intro style-two">
                    <div class="thumb">
                        <img src="{{ url('/assets/img/icons/9.png') }}" alt="img">
                    </div>
                    <h4 class="intro-title">Destinasi Wisata
                    </h4>
                    <ul>
                        <li>Air Terjun Elong Tuna</li>
                        <li>Air Terjun Babak Pelangi</li>
                        <li> Air Terjun Titian Batu Kawangan</li>
                        <li>Air Terjun Bebet Bebasak</li>
                        <li>Camping Ground Sumberan</li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-intro style-two">
                    <div class="thumb">
                        <img src="{{ url('/assets/img/icons/10.png') }}" alt="img">
                    </div>
                    <h4 class="intro-title">Produk Khas Desa
                    </h4>
                    <ul>
                        <li>Kerajinan Tangan Bambu</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-intro style-two">
                    <div class="thumb">
                        <img src="{{ url('/assets/img/icons/12.png') }}" alt="img">
                    </div>
                    <h4 class="intro-title">Makanan Khas Desa
                    </h4>
                    <ul>
                        <li>Tempeyek</li>
                        <li>Serabi</li>
                        <li> Kelepon</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-intro style-two">
                    <div class="thumb">
                        <img src="{{ url('/assets/img/icons/11.png') }}" alt="img">

                    </div>
                    <h4 class="intro-title">Fasilitas Desa
                    </h4>
                    <ul>
                        <li>Areal Parkir</li>
                        <li>Balai Pertemuan</li>
                        <li>Homestay</li>
                        <li>Kamar Mandi Umum</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- intro area End -->

<!-- about section area start -->

<!-- about section area end -->

<!-- destination-grid-area end -->




@endsection

@section('scripts')

@endsection

@section('css')

@endsection