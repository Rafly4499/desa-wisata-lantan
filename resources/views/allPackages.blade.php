@extends('layouts.frontend.master')
@section('title')
Desa Wisata Lantan - All Places
@endsection

@section('css')
<style>
    .places {
        margin-top: 60px;
        margin-bottom: 60px;
    }
</style>
@endsection



@section('content')
<!-- breadcrumb area start -->
<div class="breadcrumb-area style-two jarallax"
    style="background-image:url({{ asset('frontend/assets/img/bg/1.png') }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Packages List</h1>
                    <ul class="page-list">
                        <li><a href="/">Home</a></li>
                        <li>Packages List</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- destination area End -->
<div class="package-area pd-top-105">
    <div class="container-bg mg-top--70">
        <div class="container">
            <div class="row justify-content-center">
                @forelse ($packages as $package)

                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="single-package-card style-two wow animated fadeInUp" data-wow-duration="0.1s"
                        data-wow-delay="0.1s">
                        <div class="thumb">
                            <img src="{{ asset('storage/packageImage/'.$package->package_image) }}" alt="img">
                        </div>
                        <div class="details">
                            <h3>{{ $package->name }}</h3>
                            <ul class="package-meta">
                                <li class="tp-price-meta">
                                    <p><i class="fa fa-clock-o"></i></p>
                                    <p>Duration</p>
                                    <h4>{{ $package->day }}</h4>
                                </li>
                                <li class="tp-price-meta">
                                    <p><i class="fa fa-users"></i></p>
                                    <p>Person</p>
                                    <h4>{{ $package->people }} People</h4>
                                </li>
                                <li class="tp-price-meta">
                                    <p><i class="fa fa-tag"></i></p>
                                    <p>Price (Rp)</p>
                                    <h4>{{ $package->price }} </h4>
                                </li>
                            </ul>
                            <a href="{{ route('package.details', $package->id) }}" class="btn btn-primary">Details</a>
                            @auth
                            @if (Auth::user()->role_id == 2)
                            <a href="{{ route('package.booking', $package->id) }}" class="btn btn-gray"
                                style="margin-top:10px;">Book Now</a>
                            @endif
                            @endauth

                            @guest
                            <a href="{{ route('login') }}" class="btn btn-gray" style="margin-top:10px;">Book Now</a>
                            @endguest
                        </div>
                    </div>
                </div>
                @empty
                <h2 class="m-auto my-5 bg-info text-white text-center p-3">No Package Found. Please add some place.
                </h2>
                @endforelse

            </div>
            <div class="d-flex justify-content-between">
                <div>
                    <a href="{{ route('welcome') }}" class="btn btn-danger my-5">Back to home</a>
                </div>
                <div class="my-5">
                    {{ $packages->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection

@section('css')

@endsection