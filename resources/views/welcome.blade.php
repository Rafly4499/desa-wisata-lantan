@extends('layouts.frontend.master')
@section('title')
Desa Wisata Lantan - Home
@endsection

@section('content')
<style>
    .overlay {
        width: 100%;
        padding: 10px;
        background-color: rgba(0, 0, 0, 0.5);
    }
</style>
{{-- start banner --}}
<!-- banner area start -->
<div class="banner-area">
    <div class="banner-slider banner-slider-3">
        @foreach ($sliders as $item)
        <div class="banner-slider-item jarallax"
            style="background-image: url({{ asset('images/sliders/'. $item->photo) }});">


            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-9">
                        <div class="row">
                            <div class="col-lg-9 col-sm-8">
                                <div class="banner-inner">
                                    <p class="banner-cat s-animate-1">Discover</p>
                                    <h2 class="banner-title s-animate-2">{{ $item->title }}</h2>
                                    <p class="content s-animate-3">{{ $item->description }}</p>
                                    <a class="btn btn-yellow s-animate-4" href="{{ $item->link }}">Explore</a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-4">
                                <div class="video-popup-btn s-animate-video">
                                    <a href="{{ $item->link }}" class="video-play-btn mfp-iframe"><i
                                            class="fa fa-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- <div class="container">  -->
    <div class="banner-paginations banner-paginations-3">
        <div class="banner-slider-dots"></div>
    </div>
    <!-- </div> -->
    <div class="container">
        <div class="banner-slider-controls">
            <div class="slider-nav tp-control-nav"></div>
        </div>
    </div>
</div>
<!-- banner area end -->

<!-- search area start -->
<!-- search area end -->

<!-- upcomming tour start -->
<!-- upcomming tour end -->

<!-- upcomming tour start -->
<div class="upcomming-tour pd-top-65 pd-bottom-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <div class="section-title text-center">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">Amazing
                        Places</h2>
                    <p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">Lorem Ipsum is
                        simply dummy text the printing and typesetting industry. Lorem Ipsum has been the industry's
                        standard dummy text ever since the 1500s,</p>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($places as $place)
            <div class="col-xl-6 col-md-6">
                <div class="single-upconing-card style-two wow animated fadeInUp" data-wow-duration="0.4s"
                    data-wow-delay="0.1s">
                    <div class="shadow" style="background-image: url({{ asset('storage/place/'. $place->image) }});">
                        <img src="{{ asset('storage/place/'. $place->image) }}" alt="img">
                    </div>
                    <div class="tp-price-meta">
                        <p>Wisata</p>
                        <h6>{{ $place->district->name }}</h6>
                    </div>
                    <div class="details">
                        <h3 class="title"><a href="{{ route('place.details', $place->id) }}">{{ $place->name }}</a>
                        </h3>
                        <p>Place Type : {{ $place->placetype->name }}</p>

                    </div>
                    <div class="location">{{ $place->placetype->name }}</div>
                </div>
            </div>
            @empty
            <h2 class="my-5 bg-info text-white text-center p-3">No Place Found. Please add some place.</h2>
            @endforelse
        </div>
        <div class="innerbtn-wrap text-center wow animated fadeInUp" data-wow-duration="2.2s" data-wow-delay="0.7s">
            <a class="btn btn-yellow" href="{{ route('all.place') }}">View All Places</a>
        </div>
    </div>
</div>
<!-- upcomming tour end -->

<!-- travelus area start -->
<div class="travelus-area pd-top-120 pd-bottom-92 jarallax"
    style="background-image: url({{ asset('frontend/assets/img/bg/9.png') }});">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-8">
                <div class="section-title section-title-left-border style-two">
                    <h2 class="title">Lets Go Travel with Us</h2>
                    <p>The Best Travel for you
                    </p>
                </div>
            </div>
        </div>
        <ul class="row single-travelus-wrap-area">
            <li class="col-lg-2 col-sm-4 single-travelus-wrap">
                <div class="single-travelus text-center">
                    <div class="icons">
                        <img src="{{ asset('frontend/assets/img/icons/27.png') }}" alt="img">
                    </div>
                    <h4>Search</h4>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 single-travelus-wrap">
                <div class="single-travelus text-center">
                    <div class="icons">
                        <img src="{{ asset('frontend/assets/img/icons/28.png') }}" alt="img">
                    </div>
                    <h4>Book</h4>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 single-travelus-wrap">
                <div class="single-travelus text-center">
                    <div class="icons">
                        <img src="{{ asset('frontend/assets/img/icons/27.png') }}" alt="img">
                    </div>
                    <h4>Travel</h4>
                </div>
            </li>
            <li class="col-lg-5 offset-xl-1 single-travelus-wrap">
                <img src="{{ asset('assets/img/desawistalantan.png') }}" alt="img">
            </li>
        </ul>
        <!-- package area end -->
        <div class="package-area pd-top-105">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8">
                        <div class="section-title section-title-left-border style-two">
                            <h2 class="title">Best Packages For You</h2>
                            <p>We provides the best packages for you</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    @forelse ($packages as $package)

                    <div class="col-xl-3 col-lg-4 col-sm-6">
                        <div class="single-package-card style-two wow animated fadeInUp" data-wow-duration="0.1s"
                            data-wow-delay="0.1s">
                            <div class="thumb">
                                <img src="{{ asset('storage/packageImage/'.$package->package_image) }}" alt="img">
                            </div>
                            <div class="details">
                                <h3>{{ $package->name }}</h3>
                                <ul class="package-meta">
                                    <li class="tp-price-meta">
                                        <p><i class="fa fa-clock-o"></i></p>
                                        <p>Duration</p>
                                        <h4>{{ $package->day }}</h4>
                                    </li>
                                    <li class="tp-price-meta">
                                        <p><i class="fa fa-users"></i></p>
                                        <p>Person</p>
                                        <h4>{{ $package->people }} People</h4>
                                    </li>
                                    <li class="tp-price-meta">
                                        <p><i class="fa fa-tag"></i></p>
                                        <p>Price (Rp)</p>
                                        <h4>{{ $package->price }} </h4>
                                    </li>
                                </ul>
                                <a href="{{ route('package.details', $package->id) }}"
                                    class="btn btn-primary">Details</a>
                                @auth
                                @if (Auth::user()->role_id == 2)
                                <a href="{{ route('package.booking', $package->id) }}" class="btn btn-gray"
                                    style="margin-top:10px;">Book
                                    Now</a>
                                @endif
                                @endauth

                                @guest
                                <a href="{{ route('login') }}" class="btn btn-gray" style="margin-top:10px;">Book
                                    Now</a>
                                @endguest
                            </div>
                        </div>
                    </div>
                    @empty
                    <h2 class="m-auto my-5 bg-info text-white text-center p-3">No Package Found. Please add some
                        place.
                    </h2>
                    @endforelse

                </div>
                <div class="innerbtn-wrap text-center wow animated fadeInUp" data-wow-duration="2.2s"
                    data-wow-delay="0.7s">
                    <a class="btn btn-yellow" href="{{ route('all.package') }}">View All Packages</a>
                </div>
            </div>
        </div>
        <!-- package area end -->
    </div>
</div>
<!-- travelus area start -->

<!-- client area start -->
<div class="client-area pd-top-108">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <div class="section-title text-center">
                    <h2 class="title">What Our Clients Say</h2>
                    <p>Testimony from our client </p>
                </div>
            </div>
        </div>
        <style>
            .iFEZzR {
                display: none !important;
            }

            a[href="https://elfsight.com/google-reviews-widget/?utm_source=websites&utm_medium=clients&utm_content=google-reviews&utm_term=127.0.0.1:8000&utm_campaign=free-widget"] {
                display: none !important;
            }
        </style>
        <div class="testimony">

            <script src="https://apps.elfsight.com/p/platform.js" defer></script>
            <div class="elfsight-app-791f9452-2514-4d8a-8552-3d3dbb5d35ba"></div>
        </div>
    </div>
</div>
<!-- blog area End -->
@endsection

@section('scripts')

@endsection

@section('css')

@endsection